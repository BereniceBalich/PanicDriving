
   using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clonarGallinas : MonoBehaviour
{
    public GameObject clonePrefab;
    public int numberOfClones = 5;
    public float cloneRadius = 2f;
    public bool clonado = false;
    private void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.C) && clonado == false)
        {
            clonado = true;
            CloneAroundSelf();
        }*/
    }

    private void CloneAroundSelf()
    {
        for (int i = 0; i < numberOfClones; i++)
        {
            float angle = i * 360f / numberOfClones;
            Vector3 spawnPosition = transform.position + Quaternion.Euler(0f, angle, 0f) * Vector3.forward * cloneRadius;
            GameObject newClone = Instantiate(clonePrefab, spawnPosition, Quaternion.identity);
            newClone.transform.parent = transform.parent;
        }
    }
    public void OnCollisionEnter(Collision collision) 
    {
        if (collision.gameObject.name == "Auto2")
        {
            clonado = true;
            CloneAroundSelf();
        }
    }
}

