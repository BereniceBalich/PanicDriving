using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chanchoController : MonoBehaviour
{
    public float rollForce = 10.0f; // Fuerza para rodar el objeto
    public float maxRollAngle = 30.0f; // �ngulo m�ximo de inclinaci�n

    private Rigidbody rb;
    private bool movingRight = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // Cambiar la direcci�n de movimiento cuando el objeto alcanza el �ngulo m�ximo
        if (Mathf.Abs(transform.rotation.eulerAngles.y) >= maxRollAngle)
        {
            movingRight = !movingRight;
        }

        // Calcular la direcci�n de rodadura basada en la direcci�n actual de movimiento
        Vector3 rollDirection = movingRight ? transform.right : -transform.right;

        // Aplicar una fuerza para hacer que el objeto ruede hacia los costados
        rb.AddTorque(rollDirection * rollForce, ForceMode.Force);
    }
}