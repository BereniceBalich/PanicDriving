using UnityEngine;
using TMPro;

public class contadorDeTiempo : MonoBehaviour
{
    public float totalTime = 60f; // Tiempo total en segundos
    private float currentTime;

    public TMP_Text textMeshPro;

    private void Start()
    {
        currentTime = totalTime;
    }

    private void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            UpdateText();
        }
        else
        {
            // Aqu� puedes realizar acciones cuando el tiempo llega a cero
            currentTime = 0;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        int minutes = Mathf.FloorToInt(currentTime / 60);
        int seconds = Mathf.FloorToInt(currentTime % 60);
        textMeshPro.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}