using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cruzadores_Controller : MonoBehaviour
{
    public float movementSpeed = 5f; // Velocidad de movimiento
    public float waitTime = 2f; // Tiempo de espera al cruzar
    private bool isMoving = true;

    private void Start()
    {
        // Inicia el movimiento del animal al comenzar el juego
        MoveAnimal();
    }

    private void Update()
    {
        if (isMoving)
        {
            // Mueve el animal hacia adelante en el eje Z
            transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
        }
    }

    private void MoveAnimal()
    {
        isMoving = true;
        Invoke(nameof(StopMoving), waitTime);
    }

    private void StopMoving()
    {
        isMoving = false;

        // Gira el animal 180 grados en el eje Y (para cruzar la calle de vuelta)
        transform.Rotate(Vector3.up, 180f);

        Invoke(nameof(ResumeMoving), waitTime);
    }

    private void ResumeMoving()
    {
        isMoving = true;
        MoveAnimal();
    }
}

