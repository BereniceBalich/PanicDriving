using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderController : MonoBehaviour
{
    public Slider slider;
    public float tiempoMin = 0f;
    public float tiempo;
    public float tiempoMax;
    public bool estadoSlider = true;
    public bool iniciarCuenta = false;

    void Start()
    {
        tiempo = 0;
        slider = GetComponent<Slider>();
    }
    void FixedUpdate()
    {
        if(iniciarCuenta)
        {
          tiempo += Time.deltaTime;
          slider.value = tiempo;
        }


        if (tiempo >= tiempoMax)
        {
            tiempoMin += 30;
            tiempoMax += 30;
            slider.minValue = tiempoMin;
            slider.maxValue = tiempoMax;

            if (estadoSlider == true)
            {
                estadoSlider = false;
            }
            else
            {
                estadoSlider = true;
            }

        }
        if (estadoSlider) slider.direction = Slider.Direction.RightToLeft;
        else slider.direction = Slider.Direction.LeftToRight;

    }

}
