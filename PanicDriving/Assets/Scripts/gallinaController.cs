using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gallinaController : MonoBehaviour
{
    public Transform parentTransform;
    public GameObject objectToClone; // Objeto que deseas clonar
    public int numberOfClones = 8; // Número de clones a generar
    public float radius = 5.0f; // Radio del círculo de clonación

    public float jumpForce = 8f; // Fuerza del salto
    public float jumpInterval = 1.5f; // Intervalo entre saltos

    public LayerMask groundLayer; // Capa del suelo
    private bool isGrounded; // Bandera para detectar si el animal está en el suelo

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        InvokeRepeating(nameof(AutoJump), 0f, jumpInterval);
        
    }

    private void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < 2; i++)
            {
                //float angle = i * (360.0f / numberOfClones); // Calcula el ángulo entre cada clon
                // Vector3 offset = Quaternion.Euler(0, angle, 0) * Vector3.forward * radius; // Calcula la posición del clon

                //Vector3 clonePosition = transform.position ;

                // Clona el objeto y colócalo en la nueva posición
                GameObject clonedObject = Instantiate(objectToClone, parentTransform);
                transform.parent = parentTransform;
            }
        }*/
        // Comprobar si el animal está en el suelo
        isGrounded = Physics.Raycast(transform.position, Vector3.down, 0.1f, groundLayer);
    }

    private void AutoJump()
    {
        // Saltar si el animal está en el suelo
        if (isGrounded)
        {
            Jump();
        }
    }

    private void Jump()
    {
        // Aplicar fuerza de salto al Rigidbody
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Auto2")
        {
           
        }
    }
}
