using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyDomeController : MonoBehaviour
{
    public float scrollSpeed = 0.5f; // Velocidad de desplazamiento
    private Renderer rend;
    private Vector2 offset;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        offset = Vector2.zero;
    }

    private void Update()
    {
        // Calcula el nuevo offset basado en el tiempo transcurrido y la velocidad de desplazamiento
        offset.x += Time.deltaTime * scrollSpeed;

        // Aplica el nuevo offset al material
        rend.material.mainTextureOffset = offset;
    }
}
