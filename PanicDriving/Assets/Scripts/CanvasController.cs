using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.VisualScripting;

public class CanvasController : MonoBehaviour
{
    public GameObject CanvasGameOver;
    public GameObject CanvasMenuInicio;
    public GameObject CanvasSlider;
    public GameObject CanvasControles;
    public GameObject CanvasAssets;
    public SliderController s;

    public void btnClick()
    {
        if (this.name == "btnJugar")
        {
            CanvasMenuInicio.SetActive(false);
            CanvasSlider.SetActive(true);
            s.estadoSlider = true;
            s.iniciarCuenta = true;
            s.tiempo = 0f;

        }
        if (this.name == "btnControles")
        {
            CanvasMenuInicio.SetActive(false);
            CanvasControles.SetActive(true);

        }
        if (this.name == "btnCerrarControles")
        {
            CanvasMenuInicio.SetActive(true);
            CanvasControles.SetActive(false);

        }
        if (this.name == "btnAssets")
        {
            CanvasMenuInicio.SetActive(false);
            CanvasAssets.SetActive(true);
        }
        if (this.name == "btnCerrarAssets")
        {
            CanvasMenuInicio.SetActive(true);
            CanvasAssets.SetActive(false);
        }

    }

    
}
