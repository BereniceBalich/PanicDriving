using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class volanteController : MonoBehaviour
{

    public float tiempoMin = 0f;
    public float tiempo;
    public float tiempoMax;
    public bool ladoDelVolante = true;
   public GameObject objVolante;
     public Transform transformVolante;


    void Start()
    {
        ladoDelVolante = true;
        tiempo = 0f;
       transformVolante= objVolante.GetComponent<Transform>(); 
     
    }
    void FixedUpdate()
    {
       controlVolante();
    }
     public void controlVolante()
    {
         tiempo += Time.deltaTime;
        

        if (tiempo >= tiempoMax)
        {
            tiempoMin += 30;
            tiempoMax += 30;
           

            if (ladoDelVolante == true)
            {
             ladoDelVolante = false;
            }
            else
            {
             ladoDelVolante = true;
            }

        }
        if(ladoDelVolante)
        {
           transformVolante.position = new Vector3 (transformVolante.position.x - 0.4f ,transformVolante.position.y ,transformVolante.position.z);
        }
        else
        {
            transformVolante.position = new Vector3 (transformVolante.position.x + 0.4f ,transformVolante.position.y ,transformVolante.position.z);
        }
        
    }
}
