using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using TMPro;


public class Auto_Controller2 : MonoBehaviour
{
    public TMP_Text textMeshPro;
    public AudioClip soundClip1;
    public AudioClip soundClip2;
    public AudioClip soundClip3;
    public AudioSource audioSource;
    public bool juegoPerdido = false;
    public GameObject canvasGameOver;

    public GameObject canvasWin;
    public GameObject volanteIzq;
    public GameObject volanteDer;
    public float aceleracion = 1000f;
    public float desaceleracion = 1500f;
    public float velocidadMaxima = 50f;

    public float velocidad = 20f;
    public float velocidadRotacion = 100f;
    private Rigidbody rb;
    bool invertirJoystick;
    public int contVidas = 3;

    public SliderController s;
    
   
    public bool juegoGanado;

    public void Start()
    { 
        juegoGanado = false;
        contVidas = 3;
        inicioDeJuego();
        volanteDer.SetActive(false);
        volanteIzq.SetActive(false);

        string[] nombresJoysticks = Input.GetJoystickNames();
        foreach (string nombre in nombresJoysticks)
        {
            Debug.Log(nombre);
        }
        InvokeRepeating("CambiarJoystick", 30f, 30f);


        rb = GetComponent<Rigidbody>();
    }
    public void CambiarJoystick()
    {
        cambioDeVolante();
        if (invertirJoystick == true)
        {
            
            invertirJoystick = false;
        }
        else
        {
            
            invertirJoystick = true;
        }
    }

    public void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        textMeshPro.text =  string.Format("{0}", contVidas);
        if (contVidas == 0)
        {
            juegoPerdido = true;
            canvasGameOver.SetActive(true);
            finDeJuego();
           
        }
        if (Input.GetKey(KeyCode.R))
        {
            rb.transform.position = new Vector3(-55.89f,10.66f, 260.55f);
            rb.transform.rotation = Quaternion.Euler(0, 180, 0);
            juegoPerdido = false;
            juegoGanado = false;
            canvasGameOver.SetActive(false);
            canvasWin.SetActive(false);
            s.iniciarCuenta = true;
            s.tiempo = 0;
            contVidas = 3;
        }

            float movimientoVertical = Input.GetAxis("Vertical");
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        if (invertirJoystick == true && juegoPerdido == false && juegoGanado == false)
        {
            volanteDer.SetActive(false);
            volanteIzq.SetActive(true);
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Joystick1Button0))
               {
                movimientoVertical = 1f;
               } 
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Joystick1Button2))
                {
                    movimientoVertical = -1f;
                }
            else
                movimientoVertical = 0f;


            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Joystick2Button3))
                movimientoHorizontal = -1f;
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Joystick2Button1))
                movimientoHorizontal = 1f;
            else
                movimientoHorizontal = 0f;
        }
        else if (invertirJoystick == false && juegoPerdido == false && juegoGanado == false)
        {
            volanteDer.SetActive(true);
            volanteIzq.SetActive(false);
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Joystick2Button0))
               { movimientoVertical = 1f;
               }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Joystick2Button2))
                {movimientoVertical = -1f;
                }
            else
                movimientoVertical = 0f;


            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Joystick1Button3))
                movimientoHorizontal = -1f;
            else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Joystick1Button1))
                movimientoHorizontal = 1f;
            else
                movimientoHorizontal = 0f;
        }
        if (juegoPerdido == true)
        {
            movimientoHorizontal = 0;
            movimientoVertical = 0;
        }
        if (juegoGanado == true)
        {
            movimientoHorizontal = 0;
            movimientoVertical = 0;
            canvasWin.SetActive(true);
        }

        //Vector3 movimiento = transform.forward * movimientoVertical * velocidad;
        // rb.MovePosition(rb.position + movimiento * Time.fixedDeltaTime);
        
        if (movimientoVertical != 0f)
        {
            // Calcular la aceleración o desaceleración basada en la dirección del movimiento.
            float fuerzaActual = (movimientoVertical > 0f) ? aceleracion : desaceleracion;

            // Limitar la velocidad máxima.
            if (Mathf.Abs(rb.velocity.magnitude) < velocidadMaxima)
            {
                // Aplicar la fuerza para acelerar o desacelerar.
                rb.AddForce(transform.forward * fuerzaActual * movimientoVertical * Time.fixedDeltaTime);
            }
        }


        Quaternion rotacion = Quaternion.Euler(0, movimientoHorizontal * velocidadRotacion * Time.fixedDeltaTime, 0);
        rb.MoveRotation(rb.rotation * rotacion);
        
    }
    public void OnCollisionEnter(Collision collision)
    {
     if (collision.gameObject.tag == "Obstaculo")
     {
        contVidas -= 1;
     }

     if(collision.gameObject.tag == "condVictoria")
     {
       juegoGanado = true;
       
     }
    }
    public void OnCollisionExit(Collision collision)
    {
      
    }
    public void inicioDeJuego()
    {
        audioSource.PlayOneShot(soundClip1, 0.5f);

    }
    public void finDeJuego()
    {
        audioSource.PlayOneShot(soundClip3, 0.5f);

    }
    public void cambioDeVolante()
    {
        audioSource.PlayOneShot(soundClip2, 0.5f);

    }

}