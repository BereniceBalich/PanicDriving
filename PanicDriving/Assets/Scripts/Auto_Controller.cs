using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Auto_Controller : MonoBehaviour
{
 
    public float velocidad = 20f;
    public float velocidadRotacion = 100f;
    private Rigidbody rb;
    bool invertirJoystick;
     private bool estaColisionando = false;

    public void Start()
    {
        
        string[] nombresJoysticks = Input.GetJoystickNames();
        foreach (string nombre in nombresJoysticks)
        {
            Debug.Log(nombre);
        }
        InvokeRepeating("CambiarJoystick", 30f, 30f);


        rb = GetComponent<Rigidbody>();
    }
    public void CambiarJoystick()
    {
        if (invertirJoystick == true)
        {
            invertirJoystick = false;
        }
        else
        {
            invertirJoystick = true;
        }
    }

    public void FixedUpdate()
    {
        
        
        float movimientoVertical = Input.GetAxis("Vertical");
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        if (invertirJoystick == true)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Joystick1Button0))
                movimientoVertical = 1f;
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Joystick1Button2))
                movimientoVertical = -1f;
            else
                movimientoVertical = 0f;


            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Joystick2Button3))
                movimientoHorizontal = -1f;
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Joystick2Button1))
                movimientoHorizontal = 1f;
            else
                movimientoHorizontal = 0f;
        }
        else
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Joystick2Button0))
                movimientoVertical = 1f;
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Joystick2Button2))
                movimientoVertical = -1f;
            else
                movimientoVertical = 0f;


            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Joystick1Button3))
                movimientoHorizontal = -1f;
            else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Joystick1Button1))
                movimientoHorizontal = 1f;
            else
                movimientoHorizontal = 0f;
        }

        Vector3 movimiento = transform.forward * movimientoVertical * velocidad;
         rb.MovePosition(rb.position + movimiento * Time.fixedDeltaTime);


        Quaternion rotacion = Quaternion.Euler(0, movimientoHorizontal * velocidadRotacion * Time.fixedDeltaTime, 0);
        rb.MoveRotation(rb.rotation * rotacion);
         if (!estaColisionando)
        {
            // Devolver la velocidad normal cuando no esté colisionando.
            velocidad = 20f; // Aquí puedes ajustar el valor de la velocidad normal según tus necesidades.
        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        // Si el auto colisiona con algún objeto, podemos aplicar lógica de frenado aquí.
        // Por ejemplo, podemos reducir la velocidad del auto cuando colisiona.
       if(collision.gameObject.tag != "piso")
        if (collision.relativeVelocity.magnitude > 2f)
        {
            // Reducir la velocidad cuando la magnitud de la velocidad relativa sea mayor que 2.
            velocidad *= 0.5f;
             estaColisionando = true;
        }
    }
    public void OnCollisionExit(Collision collision)
    {
        // Cuando el auto deja de colisionar, actualizamos la variable "estaColisionando" a false.
        estaColisionando = false;
    }
   
   
}
