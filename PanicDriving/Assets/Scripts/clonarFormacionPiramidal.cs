using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clonarFormacionPiramidal : MonoBehaviour
{
    public GameObject clonePrefab;
    public int numRows = 3; // N�mero de filas en la pir�mide
    public float spacingX = 2f; // Espaciado horizontal entre clones
    public float spacingY = 2f; // Espaciado vertical entre filas de clones
    public bool clonado = false;
    private void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.P) && clonado == false)
        {
            clonado = true;
            ClonePyramidFormation();
        }*/
    }

    private void ClonePyramidFormation()
    {
        for (int row = 0; row < numRows; row++)
        {
            int numClonesInRow = numRows - row;

            for (int col = 0; col < numClonesInRow; col++)
            {
                float xOffset = spacingX * col - spacingX * numClonesInRow * 0.5f;
                float yOffset = spacingY * row;

                Vector3 spawnPosition = transform.position - transform.forward * yOffset + transform.right * xOffset;

                GameObject newClone = Instantiate(clonePrefab, spawnPosition, transform.rotation);
                newClone.transform.parent = transform; // Hacer que el clon sea hijo del objeto original
            }
        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Auto2")
        {

            clonado = true;
            ClonePyramidFormation();
        }
    }
}
