using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioClip soundClip1;
    public AudioClip soundClip2;
    public AudioClip soundClip3;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
       
        inicioDeJuego();
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

 
    public void inicioDeJuego()
    {
        audioSource.PlayOneShot(soundClip1, 0.5f);
   
    }
}
